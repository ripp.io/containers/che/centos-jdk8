FROM centos:latest

EXPOSE 4403 8080 8000 9000 22

LABEL che:server:8080:ref=web che:server:8080:protocol=http \
      che:server:9000:ref=management che:server:9000:protocol=http \
      che:server:8000:ref=tomcat8-debug che:server:8000:protocol=http

ENV JAVA_VERSION=jdk-10.0.1 \
    JAVA_HOME=/usr/local/java \
    MAVEN_VERSION=3.5.3 \
    M2_HOME=/usr/local/maven \
    MAVEN_OPTS=$JAVA_OPTS \
    TOMCAT_VERSION=8.5.31 \
    TOMCAT_HOME=/usr/local/tomcat \
    CATALINA_HOME=/usr/local/tomcat \
    PATH=$JAVA_HOME/bin:$M2_HOME/bin:$CATALINA_HOME/bin:$PATH \
    LANG=C.UTF-8 \
    TERM=xterm

RUN yum update -y \
 && yum -y install sudo openssh-server procps wget unzip mc git curl nmap vim nano \
 ## Install JAVA
 && mkdir ${JAVA_HOME} \
 && wget --no-cookies --no-check-certificate -qO- --header "Cookie: oraclelicense=accept-securebackup-cookie" \
      https://download.java.net/java/GA/jdk10/10.0.1/fb4372174a714e6b8c52526dc134031e/10/open${JAVA_VERSION}_linux-x64_bin.tar.gz \
      | tar -zx --strip-components=1 -C ${JAVA_HOME} \
 && update-alternatives --install /usr/bin/java java ${JAVA_HOME}/bin/java 100 \
 && update-alternatives --install /usr/bin/javac javac ${JAVA_HOME}/java/bin/javac 100 \
## Install Tomcat
 && mkdir ${TOMCAT_HOME} \
 && wget -qO- "http://archive.apache.org/dist/tomcat/tomcat-8/v${TOMCAT_VERSION}/bin/apache-tomcat-${TOMCAT_VERSION}.tar.gz" | tar -zx --strip-components=1 -C ${TOMCAT_HOME} \
 && rm -rf ${TOMCAT_HOME}/webapps/* \
## Install Maven
 && mkdir ${M2_HOME} \
 && wget -qO- "http://apache.mirrors.lucidnetworks.net/maven/maven-3/${MAVEN_VERSION}/binaries/apache-maven-${MAVEN_VERSION}-bin.tar.gz" | tar -zx --strip-components=1 -C ${M2_HOME} \
 && update-alternatives --install /usr/bin/mvn mvn ${M2_HOME}/bin/mvn 100 \
## Setup Permissions
 && mkdir /var/run/sshd \
 && sed -ri 's/UsePAM yes/#UsePAM yes/g' /etc/ssh/sshd_config \
 && sed -ri 's/#UsePAM no/UsePAM no/g' /etc/ssh/sshd_config \
 && echo "%wheel ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers \
 && useradd -u 1000 -G users,wheel -d /home/user --shell /bin/bash -m user \
 && usermod -p "*" user \
 && sed -i 's/requiretty/!requiretty/g' /etc/sudoers \
 && chown -R user ${TOMCAT_HOME}

USER user

RUN mkdir -p /home/user/.ssh \ 
 && ssh-keyscan github.com >> /home/user/.ssh/known_hosts \
 && ssh-keyscan gitlab.com >> /home/user/.ssh/known_hosts \
 && sudo rm -rf /var/cache/yum

WORKDIR /projects

CMD sudo /usr/bin/ssh-keygen -A \
 && sudo /usr/sbin/sshd -D \
 && /home/user/entrypoint.sh \
 && tail -f /dev/null